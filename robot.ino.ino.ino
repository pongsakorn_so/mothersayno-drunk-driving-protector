#include <pt.h>
#include <Servo.h>
#include <LiquidCrystal.h>
LiquidCrystal lcd(8, 9, 4, 5, 6, 7);

#define PT_DELAY(pt, ms, ts) \
    ts = millis(); \
    PT_WAIT_WHILE(pt, millis()-ts < (ms));

//Pin
#define switchPin 2
#define servoPin 3
#define buzzerPin 10
#define echoPin 11
#define trigPin 12
#define gasPin A1
#define soundPin A5


//driverStatus
#define AWAY 0
#define STAY 1
//doorStatus 
#define CLOSE 0
#define OPEN 1

//robotState
#define GreetSTATE 0
#define BlowSTATE 1
#define DriveSTATE 2
#define DangerSTATE 3
int robotState = 0;

//AllStatus
int driverStatus = AWAY;
int doorStatus = CLOSE;
int buzzerStatus = CLOSE;

//Value
Servo myservo;
int sw = 1;
int gasVal = 0;
int duration=0;
int distance=0;
int soundVal =0;
int SwitchVal = 0;
int blowValue = 800;
String buzzerTrigger = "0";
String val = "F";


int drunkVal = 500;
int blowVal = 1000;

struct pt pt_taskUSSensor;
struct pt pt_taskDrunkSensor;
struct pt pt_taskGasSoundSensor;
struct pt pt_taskDriveMode;
struct pt pt_taskDangerMode;
struct pt pt_taskSwitch;

struct pt pt_taskSendSerial;
struct pt pt_taskSerialEvent;


///////////////////////////////////////////////////////
void serialEvent()
{
    if (Serial1.available() > 0) {
      val = Serial1.readStringUntil('\r');
      Serial.print("value Recieve : ");
      Serial.println(val);
      if(val=="1")
        analogWrite(buzzerPin,100);
      else
        analogWrite(buzzerPin,0);
      }
      
      Serial1.flush();

}

/////////////////////////////////////////////////////////
//PT_THREAD(taskSerialEvent(struct pt* pt))
//{
//  static uint32_t ts;
//
//  PT_BEGIN(pt);
//
//  while (1)
//  {
//    if (Serial1.available() > 0) {
//      val = Serial1.readStringUntil('\r');
//      Serial.print("value Recieve : ");
//      Serial.println(val);
//      if(String(buzzerTrigger)!=val){
//        robotState = DangerSTATE;
//      }
//      Serial1.flush();
//    }
//    PT_DELAY(pt, 2000, ts);
//  }
//  PT_END(pt);
//}

///////////////////////////////////////////////////////
void sendSerial(){
  String sendData = "";
  sendData += String(gasVal);
  Serial1.print(sendData);
  Serial1.print('\r');
  Serial.print("sendDATA : ");
  Serial.println(sendData);
  Serial.print('\r');
  
}

///////////////////////////////////////////////////////
PT_THREAD(taskSendSerial(struct pt* pt))
{
  static uint32_t ts;

  PT_BEGIN(pt);

  while (1)
  {
    //if(robotState==DangerSTATE)
    sendSerial();
      
    PT_DELAY(pt, 600, ts);
  }

  PT_END(pt);
}

///////////////////////////////////////////////////////
PT_THREAD(taskUSSensor(struct pt* pt))
{
  static uint32_t ts;

  PT_BEGIN(pt);

  while (1)
  {
    digitalWrite(trigPin, LOW);
    delayMicroseconds(2);
    digitalWrite(trigPin, HIGH);
    delayMicroseconds(10);
    digitalWrite(trigPin, LOW);
    duration = pulseIn(echoPin, HIGH);
    distance = (duration/2) / 29.1;
    
    if(distance<=80&&robotState==GreetSTATE){
      driverStatus=STAY;
      lcd.clear();
      lcd.setCursor(0,0);
      lcd.print("WELCOME");
    }else if(distance>20&&robotState==GreetSTATE){
      driverStatus=AWAY;
      lcd.clear();
      lcd.setCursor(0,0);
      lcd.print("BYEBYE");
    }
    
    PT_DELAY(pt, 100, ts);
  }

  PT_END(pt);
}

///////////////////////////////////////////////////////
PT_THREAD(taskGasSoundSensor(struct pt* pt))
{
  static uint32_t ts;

  PT_BEGIN(pt);
  
  while (1)
  {
    if((robotState==GreetSTATE&&driverStatus==STAY)||robotState==DriveSTATE){
      gasVal = analogRead(gasPin);
      soundVal = analogRead(soundPin);
    }
    PT_DELAY(pt,100,ts);
  }

  PT_END(pt);
}

///////////////////////////////////////////////////////
PT_THREAD(taskDrunkSensor(struct pt* pt))
{
  static uint32_t ts;

  PT_BEGIN(pt);
  
  while (1)
  {
     Serial.println(soundVal);
//   Serial.println(robotState);
    if(soundVal >= blowValue&&robotState==GreetSTATE){
      robotState = BlowSTATE;
      analogWrite(buzzerPin,100);
      PT_DELAY(pt,1500,ts);
      analogWrite(buzzerPin,0);
      doResult(gasVal);
    }

    PT_DELAY(pt,100,ts);
  }
  PT_END(pt);
}

///////////////////////////////////////////////////////
PT_THREAD(taskDriveMode(struct pt* pt))
{
  static uint32_t ts;
  PT_BEGIN(pt);

  while (1)
  {
    if(robotState==DriveSTATE){
      lcd.clear();
      lcd.setCursor(0,0);
      lcd.print("DRIVEMODE");
    
      if(gasVal>500){
        robotState=DangerSTATE;
      }
    }
    PT_DELAY(pt, 100, ts);
  }

  PT_END(pt);
}

///////////////////////////////////////////////////////
PT_THREAD(taskDangerMode(struct pt* pt))
{
  static uint32_t ts;
  PT_BEGIN(pt);

  while (1)
  {
    if(robotState==DangerSTATE){
      analogWrite(buzzerPin,100);
      lcd.clear();
      lcd.setCursor(5,0);
      lcd.print("DANGER");
      lcd.setCursor(5,1);
      lcd.print("DANGER");
    }
    PT_DELAY(pt, 100, ts);
  }

  PT_END(pt);
}

///////////////////////////////////////////////////////
PT_THREAD(taskSwitch(struct pt* pt))
{
  static uint32_t ts;
  PT_BEGIN(pt);

  while (1)
  {
    sw = digitalRead(2);
    if(sw==0){
      lcd.clear();
      lcd.setCursor(5,0);
      buzzerTrigger=0;
      analogWrite(buzzerPin,0);
      closeDoor();
      robotState = GreetSTATE;
    }
    PT_DELAY(pt, 100, ts);
  }

  PT_END(pt);
}

///////////////////////////////////////////////////////
void doResult(int gasVal){
  String result = "NaN";
  if(gasVal>drunkVal){
    result = "You are drunk";
    robotState=GreetSTATE;
  }
  else{
    result= "PASS";
    openDoor();
    robotState=DriveSTATE;
  }
  
  lcd.clear();
  lcd.setCursor(0,0);
  lcd.print("GAS VALUE:");
  lcd.setCursor(0,1);
  lcd.print(gasVal);
  delay(3000);
 
  lcd.clear();
  lcd.setCursor(0,0);
  lcd.print(result);
  delay(3000);
}

///////////////////////////////////////////////////////
void openDoor(){
    myservo.write(170);
    doorStatus=OPEN;
}

///////////////////////////////////////////////////////
void closeDoor(){
    myservo.write(10);
    doorStatus=CLOSE;
}

///////////////////////////////////////////////////////
void setup()
{
  Serial1.begin(115200);
  Serial.begin(9600);
  lcd.begin(16, 2);
  pinMode(gasPin, INPUT);
  pinMode(trigPin, OUTPUT);
  pinMode(echoPin, INPUT);
  pinMode(soundPin,INPUT);
  PT_INIT(&pt_taskUSSensor);
  PT_INIT(&pt_taskGasSoundSensor);
  PT_INIT(&pt_taskDrunkSensor);
  PT_INIT(&pt_taskDriveMode);
  PT_INIT(&pt_taskDangerMode);
  PT_INIT(&pt_taskSwitch);
  PT_INIT(&pt_taskSendSerial);
  PT_INIT(&pt_taskSerialEvent);
  myservo.attach(servoPin);
  
}

///////////////////////////////////////////////////////
void loop() {
  taskUSSensor(&pt_taskUSSensor);
  taskGasSoundSensor(&pt_taskGasSoundSensor);
  taskDrunkSensor(&pt_taskDrunkSensor);
  taskDriveMode(&pt_taskDriveMode);
  taskDangerMode(&pt_taskDangerMode);
  taskSwitch(&pt_taskSwitch);
  
  serialEvent();
  //taskSerialEvent(&pt_taskSerialEvent);
  taskSendSerial(&pt_taskSendSerial);
}
